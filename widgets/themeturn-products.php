<?php 

// Adds widget: Latest Products
class Latestproducts_Widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'latestproducts_widget',
			esc_html__( 'TLatest Products', 'themeturn-companion' ),
			array( 'description' => esc_html__( 'ProductsWidget', 'themeturn-companion' ), ) // Args
		);
	}

	private $widget_fields = array(
		array(
			'label' => 'Number of Posts',
			'id' => 'verum_no_posts',
			'default' => '3',
			'type' => 'number',
		),
		array(
			'label' => 'Display Thumbnail',
			'id' => 'verum_display_thumb',
			'default' => '1',
			'type' => 'checkbox',
		)
	);

	public function widget( $args, $instance ) {
		echo wp_kses_post($args['before_widget']);

		if ( ! empty( $instance['title'] ) ) {
			echo wp_kses_post( $args['before_title']) . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		// Output generated fields
		$product_args = array(
			'category__in' => wp_get_post_categories(),
			'post_type'			=> 'download',
			'posts_per_page'	=> $instance['verum_no_posts'],
		);
		$products = new WP_Query( $product_args );

    while ( $products->have_posts() ) : $products->the_post(); 
    ?>

    <div class="product-item">
    	<?php 
			if($instance['verum_display_thumb']):
		 ?>

    	<div class="product-thumb">
    		<a href="<?php the_permalink(); ?>">
    			<?php the_post_thumbnail('sidebar-product-img', array('class' => 'img-fluid')) ?>
    		</a>
    	</div>

    	<?php endif; ?>
    	
    	<h5>
    		<a href="<?php the_permalink(); ?>">
    			 <?php echo wp_trim_words( get_the_title(), 7, '' );?>
    		</a>
    	</h5>
    </div>

     <?php 
 		endwhile; 
 		wp_reset_query();
		
		echo wp_kses_post($args['after_widget']);
	}

	public function field_generator( $instance ) {
		$output = '';
		foreach ( $this->widget_fields as $widget_field ) {
			$default = '';
			if ( isset($widget_field['default']) ) {
				$default = $widget_field['default'];
			}
			$widget_value = ! empty( $instance[$widget_field['id']] ) ? $instance[$widget_field['id']] : esc_html__( $default, 'themeturn-companion' );
			switch ( $widget_field['type'] ) {
				default:
					$output .= '<p>';
					$output .= '<label for="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'">'.esc_attr( $widget_field['label'], 'themeturn-companion' ).':</label> ';
					$output .= '<input class="widefat" id="'.esc_attr( $this->get_field_id( $widget_field['id'] ) ).'" name="'.esc_attr( $this->get_field_name( $widget_field['id'] ) ).'" type="'.$widget_field['type'].'" value="'.esc_attr( $widget_value ).'">';
					$output .= '</p>';
			}
		}
		echo $output;
	}

	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( '', 'themeturn-companion' );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'themeturn-companion' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
		$this->field_generator( $instance );
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		foreach ( $this->widget_fields as $widget_field ) {
			switch ( $widget_field['type'] ) {
				default:
					$instance[$widget_field['id']] = ( ! empty( $new_instance[$widget_field['id']] ) ) ? strip_tags( $new_instance[$widget_field['id']] ) : '';
			}
		}
		return $instance;
	}
}

function register_latestproducts_widget() {
	register_widget( 'Latestproducts_Widget' );
}
add_action( 'widgets_init', 'register_latestproducts_widget' );