<?php
/*
Plugin Name: Themeturn Companion
Plugin URI:
Description: Themeturn Companion Plugin
Version: 1.0
Author: Esrat
Author URI: https://themeturn.com
License: GPLv2 or later
Text Domain: themeturn-companion
Domain Path: /languages/
*/

require_once plugin_dir_path(__FILE__)."/widgets/verum-social-widget.php";
require_once plugin_dir_path(__FILE__)."/widgets/verum-flickr-widget.php";
require_once plugin_dir_path(__FILE__)."/widgets/verum-about.php";
require_once plugin_dir_path(__FILE__)."/widgets/verum-latest-post.php";
require_once plugin_dir_path(__FILE__)."/widgets/verum-adv-widget.php";
require_once plugin_dir_path(__FILE__)."/widgets/verum-newsletter-widget.php";
require_once plugin_dir_path(__FILE__)."/widgets/themeturn-products.php";
// require_once plugin_dir_path(__FILE__)."/widgets/flickr-widget.php";
// require_once plugin_dir_path(__FILE__)."/widgets/verum-recent-posts-widget.php";

function verumc_load_textdomain(){
	load_plugin_textdomain('themeturn-companion',false,dirname(__FILE__)."/languages");
}
add_action('plugins_loaded','verumc_load_textdomain');



function theme_plugin_init() {
  add_image_size( 'verum-latest-thumb', 85,85, true );
}
add_action('init', 'theme_plugin_init');

   